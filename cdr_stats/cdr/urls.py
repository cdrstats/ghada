#
# CDR-Stats License
# http://www.cdr-stats.org
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2011-2012 Star2Billing S.L.
#
# The Initial Developer of the Original Code is
# Arezqui Belaid <info@star2billing.com>
#
from django.conf.urls import patterns, url

urlpatterns = patterns('cdr.views',
    (r'^$', 'index'),
    (r'^index/$', 'index'),
    (r'^cdr_view/$', 'cdr_view'),
    (r'^cdr_export_csv/$', 'cdr_export_to_csv'),
    (r'^cdr_detail/(?P<id>\w+)/(?P<switch_id>\w+)/$', 'cdr_detail'),
    url(r'^dashboard/$', 'cdr_dashboard',name="dashboard"),
    (r'^hourly_report/$', 'cdr_report_by_hour'),
    (r'^weekly_comparison/$', 'cdr_weekly_comparison'),
    (r'^cdr_overview/$', 'cdr_overview'),
    (r'^cdr_concurrent_calls/$', 'cdr_concurrent_calls'),
    (r'^cdr_realtime/$', 'cdr_realtime'),
    (r'^mail_report/$', 'mail_report'),
    (r'^country_report/$', 'cdr_country_report'),
    (r'^world_map/$', 'world_map_view'),
    url(r'^bokeh/$', 'ma_fonction_bokeh',name="ma_premiere_page"),
    url(r'^number/(?P<numero>\d+)/$', 'plot_by_num',name="Weekly comparison by accountcode"),
    url(r'^numero/(?P<numero>\d+)/$', 'plot_by_accountcode', name="plot_by_accountcode"),
    url(r'^concurrent.calls/$', 'number_calls',name="Concurrent call"),
    url(r'^ACD_numero/(?P<numero>\d+)/$', 'Bokeh_byNum',name="Average Call Duration"),
    url(r'^CONC_numero/(?P<numero>\d+)/$', 'CONCCalls_byACCT',name="Concurrent Call"),
    url(r'^Hourly_Concurrent_Call/$', 'calls_byHour',name="Concurrent Call"),
    url(r'^Hourly_Concurrent_Call_byNumber/(?P<numero>\d+)/$', 'calls_hour_byNum',name="Concurrent Call"),
    url(r'^Weekly_Comparison_Concurrent_Call/$', 'calls_weekly',name="Concurrent Call weekly comparison"),
    url(r'^Weekly_Concurrent_Call_byNumber/(?P<numero>\d+)/$', 'calls_weekly_byNum',name="Concurrent Call weekly_comparison")
   # (r'^get_realtime_json/$', 'cdr.ajax.get_realtime_json'),

)
